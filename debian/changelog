tinyexr (1.0.10+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.10+dfsg
  * Refresh patches (no functional changes)

 -- Timo Röhling <roehling@debian.org>  Fri, 07 Feb 2025 13:08:36 +0100

tinyexr (1.0.9+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.9+dfsg
  * Refresh patches (no functional changes)
  * Bump Standards-Version to 4.7.0

 -- Timo Röhling <roehling@debian.org>  Fri, 30 Aug 2024 14:43:14 +0200

tinyexr (1.0.8+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.8+dfsg
  * Update patches
    - Drop 0005-Properly-check-for-Big-Endian.patch (merged upstream)

 -- Timo Röhling <roehling@debian.org>  Sat, 24 Feb 2024 00:28:19 +0100

tinyexr (1.0.7+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.7+dfsg
  * Refresh patches

 -- Timo Röhling <roehling@debian.org>  Sat, 05 Aug 2023 22:17:11 +0200

tinyexr (1.0.6+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.6+dfsg
  * Pass version numbers from d/rules to CMake

 -- Timo Röhling <roehling@debian.org>  Fri, 30 Jun 2023 08:59:27 +0200

tinyexr (1.0.5+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.5+dfsg
  * Refresh patches
    - Drop CVE patches which have been merged upstream
  * Bump Standards-Version to 4.6.2
  * Update symbols

 -- Timo Röhling <roehling@debian.org>  Fri, 16 Jun 2023 23:37:22 +0200

tinyexr (1.0.1+dfsg-4) unstable; urgency=medium

  * Fix vulnerabilities
    - CVE-2022-34300: Heap overflow in DecodePixelData
    - CVE-2022-38529: Heap overflow in rleUncompress
  * Bump Standards-Version to 4.6.1
  * Fix cross-compilation

 -- Timo Röhling <roehling@debian.org>  Thu, 08 Sep 2022 20:50:54 +0200

tinyexr (1.0.1+dfsg-3) unstable; urgency=medium

  * Fix d/watch to use tags, not releases on Github

 -- Timo Röhling <roehling@debian.org>  Tue, 26 Oct 2021 22:37:30 +0200

tinyexr (1.0.1+dfsg-2) unstable; urgency=medium

  * Fix FTBFS on BE architectures

 -- Timo Röhling <roehling@debian.org>  Tue, 31 Aug 2021 13:42:23 +0200

tinyexr (1.0.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.1+dfsg
  * Update maintainer address
  * Bump Standards-Version to 4.6.0

 -- Timo Röhling <roehling@debian.org>  Sun, 29 Aug 2021 20:43:34 +0200

tinyexr (1.0.0+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #974927)

 -- Timo Röhling <timo@gaussglocke.de>  Tue, 17 Nov 2020 19:42:31 +0100
